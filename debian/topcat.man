.TH TOPCAT "1" "March 2017" "topcat " "User Commands"
.SH NAME
topcat \- Tool for OPerations on Catalogues And Tables
.SH SYNOPSIS
.B topcat
\fI\,<flags> \/\fR[[\fI\,-f <format>\/\fR] \fI\,<table> \/\fR...]
.SH DESCRIPTION
TOPCAT is an interactive graphical viewer and editor for tabular
data. Its aim is to provide most of the facilities that astronomers
need for analysis and manipulation of source catalogues and other
tables, though it can be used for non-astronomical data as well. It
understands a number of different astronomically important formats
(including FITS and VOTable) and more formats can be added.
.SH OPTIONS
.TP
\fB\-f\fR <format>
File format. Auto\-detected formats: fits\-plus, colfits\-plus,
colfits\-basic, fits, votable
.IP
All known formats: fits\-plus, colfits\-plus, colfits\-basic, fits,
votable, ascii, csv, tst, ipac, wdc
.TP
\fB\-verbose\fR
Increase verbosity of reports to console
.TP
\fB\-debug\fR
Add debugging information to console log messages
.TP
\fB\-demo\fR
Start with demo data
.TP
\fB\-running\fR
Use existing TOPCAT instance if one is running
.TP
\fB\-memory\fR
Use memory storage for tables
.TP
\fB\-disk\fR
Use disk backing store for large tables
.TP
\fB\-samp\fR
Use SAMP for tool interoperability
.TP
\fB\-[no]hub\fR
[don't] run internal SAMP hub
.TP
\fB\-exthub\fR
Run external SAMP hub
.TP
\fB\-noserv\fR
Don't run any services (SAMP)
.TP
\fB\-help\fR
Print help message and exit
.TP
\fB\-version\fR
Print component versions etc and exit
.TP
\fB\-stilts\fR <args>
Run STILTS, not TOPCAT
.TP
\fB\-jsamp\fR <args>
Run JSAMP not TOPCAT
.IP
Useful Java flags:
.TP
\fB\-classpath\fR jar1:jar2..
Specify additional classes
.TP
\fB\-XmxnnnM\fR
use nnn megabytes of memory
.TP
\fB\-Dname\fR=\fI\,value\/\fR
Set system property
.SH Properties
Useful system properties (\fB\-Dname\fR=\fI\,value\/\fR \- lists are colon\-separated):
.TP
\fBjava.io.tmpdir\fR
temporary filespace directory
.TP
\fBjdbc.drivers\fR
JDBC driver classes
.TP
\fBjel.classes\fR
custom algebraic function classes
.TP
\fBjel.classes.activation\fR
custom action function classes
.TP
\fBstar.connectors\fR
custom remote filestore classes
.TP
\fBstartable.load.dialogs\fR
custom load dialogue classes
.TP
\fBstartable.readers\fR
custom table input handlers
.TP
\fBstartable.writers\fR
custom table output handlers
.TP
\fBstartable.storage\fR
storage policy
.TP
\fBmark.workaround\fR
work around mark/reset bug (see topcat \fB\-jsamp\fR \fB\-help\fR for more)
.SH VERSION
This is the Debian version of topcat. See /usr/share/doc/topcat/README.Debian
for differences.
.SH AUTHOR
Mark Taylor (Bristol University)
.SH "SEE ALSO"
WWW: http://www.starlink.ac.uk/topcat/
