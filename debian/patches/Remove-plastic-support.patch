From: Ole Streicher <olebole@debian.org>
Date: Thu, 5 Jan 2017 13:11:20 +0100
Subject: Remove plastic support

Plastic is very outdated now, and should not be used anymore. This enables
to remove a number of dependencies.
---
 build.xml                                         | 19 ++++-
 src/docs/sun253.xml                               | 99 ++++++-----------------
 src/main/uk/ac/starlink/topcat/ControlWindow.java |  6 --
 src/main/uk/ac/starlink/topcat/Driver.java        | 15 +---
 4 files changed, 42 insertions(+), 97 deletions(-)

diff --git a/build.xml b/build.xml
index 02ea19a..14a3fa1 100644
--- a/build.xml
+++ b/build.xml
@@ -452,7 +452,13 @@
       <exclude name="**/{Retired2.java}"/>
       <exclude name="**/*.html"/>
       <exclude name="**/*.properties*"/>
-    </javac>
+
+      <exclude name="uk/ac/starlink/topcat/interop/TopcatPlasticListener.java"/>
+      <exclude name="uk/ac/starlink/topcat/interop/PlasticCommunicator.java"/>
+      <exclude name="uk/ac/starlink/topcat/interop/SelectivePlasticListModel.java"/>
+      <exclude name="uk/ac/starlink/topcat/interop/TopcatTransmitter.java"/>
+      <exclude name="uk/ac/starlink/topcat/interop/PlasticImageActivity.java"/>
+   </javac>
 
     <!-- Copy extra files that should live with packages classes
      !   (i.e. are discovered using "getResource()"). -->
@@ -1219,10 +1225,15 @@
 
       <!-- Get a list of directories that name all the potential
        !   java packages -->
-      <packageset dir="${java.dir}" defaultexcludes="yes">
-         <include name="**"/>
+      <fileset dir="${java.dir}" defaultexcludes="yes">
+         <include name="**/*.java"/>
          <exclude name="uk/ac/starlink/topcat/doc"/>
-      </packageset>
+	 <exclude name="uk/ac/starlink/topcat/interop/TopcatPlasticListener.java"/>
+	 <exclude name="uk/ac/starlink/topcat/interop/PlasticCommunicator.java"/>
+	 <exclude name="uk/ac/starlink/topcat/interop/SelectivePlasticListModel.java"/>
+	 <exclude name="uk/ac/starlink/topcat/interop/TopcatTransmitter.java"/>
+	 <exclude name="uk/ac/starlink/topcat/interop/PlasticImageActivity.java"/>
+      </fileset>
 
       <!-- Link to the full Java API at SUNs website -->
       <link offline="true" href="${javaapi.url}"
diff --git a/src/docs/sun253.xml b/src/docs/sun253.xml
index a3db78a..e255e03 100644
--- a/src/docs/sun253.xml
+++ b/src/docs/sun253.xml
@@ -2105,7 +2105,7 @@ The windows from which this is done are documented in <ref id="vo-windows"/>.
 </p>
 
 <p><strong>Note:</strong>
-For information on SAMP or PLASTIC, which are protocols developed
+For information on SAMP, which is a protocol developed
 within the Virtual Observatory context, but are not necessarily 
 concerned with remote data access, see <ref id="interop"/>.
 </p>
@@ -3236,7 +3236,7 @@ messaging protocols:
 <li><webref url="http://www.ivoa.net/samp/">SAMP</webref>
     (Simple Applications Messaging Protocol)</li>
 <li><webref url="http://plastic.sourceforge.net/">PLASTIC</webref>
-    (PLatform for AStronomical InterConnection</li>
+    (PLatform for AStronomical InterConnection, <em>not available in the Debian version</em>)</li>
 </ul>
 An example of the kind of thing which can be done might be:
 <ol>
@@ -3272,9 +3272,8 @@ and others.
 the same way.  SAMP is an evolution of PLASTIC with a number of
 technical improvements, and PLASTIC has been deprecated in favour
 of SAMP since around 2009.
-PLASTIC is therefore of mainly historical interest,
-though TOPCAT can still run in PLASTIC mode on request,
-if you need it to communicate with older tools that can only speak PLASTIC.
+PLASTIC is therefore of mainly historical interest <em>and not implemented
+in the Debian version.</em>
 </p>
 
 <p>The communication architecture of the two protocols is basically the same:
@@ -3286,18 +3285,6 @@ it will connect to it automatically.
 If no SAMP hub is running, TOPCAT will set one up during application startup.
 </p>
 
-<p>TOPCAT can work in either SAMP or PLASTIC mode, but not both at once.
-It determines which mode to use at startup:
-if the <code>-samp</code> or <code>-plastic</code> flag is supplied
-on the command line the corresponding mode will be used;
-otherwise it will try to use SAMP.
-It is easy to tell which mode is being used by looking at the 
-<ref id="ControlWindow">Control Window</ref>;
-in SAMP mode the <ref id="sampPanel">SAMP panel</ref> displaying connection
-and message status is visible at the bottom of the right hand panel
-(there are a few other changes to the GUI as well).
-</p>
-
 <p>This communication has two aspects to it: on the one hand
 TOPCAT can send messages to other applications which causes them
 to do things, and on the other hand TOPCAT can receive and act on
@@ -3308,7 +3295,7 @@ The
 messages are described separately in the subsections below.
 There are also sections on the, somewhat different, 
 ways to control and monitor messaging operatiion
-for the cases of SAMP and PLASTIC.
+for the case of SAMP.
 </p>
 
 <p>Note that the new <ref id="activation">activation action</ref>
@@ -3376,9 +3363,8 @@ by other SAMP applications.
 <subsect id="plasticControl">
 <subhead><title>PLASTIC control</title></subhead>
 
-<p><em>Note: the PLASTIC protocol has been deprecated in favour
-of SAMP since about 2009, and this functionality, though still present,
-is of mostly historical interest.</em>
+<p><em>Note: the PLASTIC protocol is not implemented in the Debian version
+of TOPCAT</em>
 </p>
 
 <p>You can view and control operations relating to the PLASTIC hub
@@ -3437,9 +3423,8 @@ It contains the following options:
 <subhead><title>Messages Transmitted</title></subhead>
 
 <p>This section describes the messages which TOPCAT can transmit to other 
-tools which understand the SAMP or PLASTIC protocol, and how to cause them
-to be sent.  Approximately the same behaviour results for either
-SAMP or PLASTIC, except as noted.
+tools which understand the SAMP protocol, and how to cause them
+to be sent.
 </p>
 
 <p>In most cases you can choose two ways to transmit a message
@@ -3469,7 +3454,7 @@ which claim to support the relevant message.
 </p>
 
 <p>Below is a list of places you can cause TOPCAT to transmit messages.
-The SAMP <em>MTypes</em> and PLASTIC <em>message IDs</em> 
+The SAMP <em>MTypes</em> <em>message IDs</em>
 are listed along with the descriptions;
 unless you are a tool developer you can probably ignore these.
 
@@ -3490,10 +3475,6 @@ unless you are a tool developer you can probably ignore these.
     <p>SAMP MTypes:
           <code>table.load.votable</code> or
           <code>table.load.fits</code>
-    </p>
-    <p>PLASTIC Message IDs:
-          <code>ivo://votech.org/votable/load</code> or
-          <code>ivo://votech.org/votable/loadFromURL</code>
     </p></dd>
 
 <dt>Transmit Subset</dt>
@@ -3511,7 +3492,7 @@ unless you are a tool developer you can probably ignore these.
     (1) the table has been loaded from the same URL/filename by the 
     other tool(s) or
     (2) the other tool(s) have acquired the table because it has 
-    already been broadcast using SAMP/PLASTIC.
+    already been broadcast using SAMP.
     </p>
     <p>Also, whenever a new subset is created, for instance by entering an
     algebraic expression or tracing out a region on a plot
@@ -3521,8 +3502,6 @@ unless you are a tool developer you can probably ignore these.
     the table's subset list.
     </p>
     <p>SAMP MType: <code>table.select.rowList</code>
-    </p>
-    <p>PLASTIC Message ID: <code>ivo://votech.org/votable/showObjects</code>
     </p></dd>
 
 <dt>Transmit Row</dt>
@@ -3559,8 +3538,6 @@ unless you are a tool developer you can probably ignore these.
     and this can be sent to image applications from its Interop menu.
     </p>
     <p>SAMP MType: <code>image.load.fits</code>
-    </p>
-    <p>PLASTIC Message ID: <code>ivo://votech.org/fits/image/loadFromURL</code>
     </p></dd>
 
 <dt>Transmit Spectrum</dt>
@@ -3577,7 +3554,6 @@ unless you are a tool developer you can probably ignore these.
     present in most of the Virtual Observatory windows allows
     you to send lists of VO registry resource identifiers to other
     applications which can make use of them.
-    Note this only works in SAMP mode, not for PLASTIC.
     </p>
     <p>SAMP MTypes: <code>voresource.loadlist.cone</code>,
                     <code>voresource.loadlist.siap</code>,
@@ -3598,8 +3574,8 @@ unless you are a tool developer you can probably ignore these.
 <subhead><title>Messages Received</title></subhead>
 
 <p>This section describes the messages which TOPCAT will respond to 
-when it receives them from other applications via the SAMP/PLASTIC hub.
-The SAMP MTypes and PLASTIC message IDs are listed along with the descriptions;
+when it receives them from other applications via the SAMP hub.
+The SAMP MTypes are listed along with the descriptions;
 unless you are a tool developer you can probably ignore these.
 
 <dl>
@@ -3615,10 +3591,6 @@ unless you are a tool developer you can probably ignore these.
        <code>table.load.pds4</code> or
        <code>table.load.stil</code>
     </p>
-    <p>PLASTIC Message ID:
-       <code>ivo://votech.org/votable/load</code> or
-       <code>ivo://votech.org/votable/loadFromURL</code>
-    </p>
     <p>Note the non-standard MType <code>table.load.stil</code> is
     supported for loading tables with SAMP.  This is like the other
     <code>table.load.*</code> MTypes, but any table format supported
@@ -3648,8 +3620,6 @@ unless you are a tool developer you can probably ignore these.
     treated may be made available in future versions.
     </p>
     <p>SAMP MType: <code>table.select.rowList</code>
-    </p>
-    <p>PLASTIC Message ID: <code>ivo://votech.org/votable/showObjects</code>
     </p></dd>
 
 <dt>Highlight Row</dt>
@@ -3662,8 +3632,6 @@ unless you are a tool developer you can probably ignore these.
     in the <ref id="ActivationWindow">Activation Window</ref>.
     </p>
     <p>SAMP MType: <code>table.highlight.row</code>
-    </p>
-    <p>PLASTIC Message ID: <code>ivo://votech.org/votable/highlightObject</code>
     </p></dd>
 
 <dt>Load Resource Identifiers</dt>
@@ -3716,15 +3684,6 @@ For SAMP these are:
 <li><code>samp.hub.event.subscriptions</code></li>
 <li><code>samp.app.ping</code></li>
 </ul>
-and for PLASTIC:
-<ul>
-<li><code>ivo://votech.org/test/echo</code></li>
-<li><code>ivo://votech.org/info/getName</code></li>
-<li><code>ivo://votech.org/info/getIconURL</code></li>
-<li><code>ivo://votech.org/hub/event/ApplicationRegistered</code></li>
-<li><code>ivo://votech.org/hub/event/ApplicationUnregistered</code></li>
-<li><code>ivo://votech.org/hub/event/HubStopping</code></li>
-</ul>
 </p>
 
 </subsect>
@@ -3912,29 +3871,23 @@ The meaning of the flags is as follows:
 
 <dt>-samp</dt>
 <dd><p>Forces TOPCAT to use SAMP for tool interoperability
-    (see <ref id="interop"/>).  SAMP rather than PLASTIC is the
-    default, so this flag has no effect.
-    </p></dd>
-
-<dt>-plastic</dt>
-<dd><p>Forces TOPCAT to use PLASTIC instead of SAMP for tool
-    interoperability (see <ref id="interop"/>).
+    (see <ref id="interop"/>).  SAMP is always used, so this flag has no effect.
     </p></dd>
 
 <dt>-[no]hub</dt>
 <dd><p>The <code>-hub</code> flag causes TOPCAT to run an internal
-    SAMP or PLASTIC hub (SAMP by default), if no hub is already
+    SAMP hub, if no hub is already
     running, and the <code>-nohub</code> flag prevents this from happening.
     The default behaviour, when neither of these flags is given,
     is to start a hub automatically for
-    SAMP but not for PLASTIC.  The hub will terminate when TOPCAT does,
+    SAMP.  The hub will terminate when TOPCAT does,
     or can be shut down manually with the 
     <label>Interop|Stop Internal Hub</label> (&IMG.NO_HUB;) menu item.
     See <ref id="interop"/>.
     </p></dd>
 
 <dt>-exthub</dt>
-<dd><p>Causes TOPCAT to attempt to run an external SAMP or PLASTIC hub,
+<dd><p>Causes TOPCAT to attempt to run an external SAMP hub,
     if no hub
     is already running.  The hub will show up in its own window, and can
     be stopped by closing the window.  The hub will continue to run 
@@ -3945,8 +3898,8 @@ The meaning of the flags is as follows:
     </p></dd>
 
 <dt>-noserv</dt>
-<dd><p>Prevents TOPCAT from running any services.  Currently the services
-    which it may run are SAMP or PLASTIC (see above).
+<dd><p>Prevents TOPCAT from running any services.  Currently the service
+    which it may run is SAMP (see above).
     </p></dd>
 
 <dt>-checkvers</dt>
@@ -5560,7 +5513,7 @@ of the current status of SAMP communications.
 For a discussion of the whats and whys of SAMP, see
 <ref id="interop"/>.
 <strong>Note</strong> that if not running in SAMP mode
-(e.g. if in PLASTIC or no-server mode) this panel will not appear.
+(e.g. if in no-server mode) this panel will not appear.
 SAMP mode is the default under normal circumstances.
 </p>
 
@@ -5630,7 +5583,7 @@ functions.  They are grouped as follows:
 
     <dt>&IMG.BROADCAST; Broadcast Table</dt>
     <dd><p>Broadcasts the current <label>Apparent Table</label> to any
-        applications registered using the SAMP or PLASTIC protocol.
+        applications registered using the SAMP protocol.
         See <ref id="interop"/>.
         </p></dd>
 
@@ -5831,7 +5784,7 @@ and those common to other windows (described in <ref id="commonMenus"/>).
 
 <dt>&IMG.SEND; Send Table to ...</dt>
 <dd><p>Sends the currently selected table to a selected listening application
-    using SAMP or PLASTIC.  Select the desired recipient application from the
+    using SAMP.  Select the desired recipient application from the
     submenu.</p></dd>
 
 <dt>&IMG.LOG; View Log</dt>
@@ -6006,7 +5959,7 @@ the <label>Joins</label> menu.
 </p>
 
 <p>The <label>Interop</label> menu contains options relevant to 
-SAMP (or possibly PLASTIC) tool interoperability;
+SAMP tool interoperability;
 see <ref id="interop"/>:
 
 <dl>
@@ -6856,7 +6809,7 @@ can be edited by double-clicking on them in the normal way.
     </p></dd>
 
 <dt>&IMG.BROADCAST; Broadcast Subset</dt>
-<dd><p>Causes the selected subset to be broadcast using SAMP or PLASTIC to other
+<dd><p>Causes the selected subset to be broadcast using SAMP to other
     listening applications.  Any other listening application which is
     displaying the same table is then invited to highlight the selection
     of rows corresonding to the selected subset.
@@ -19826,9 +19779,9 @@ or manually as described in <ref id="axisConfig"/>.
 displayed image for external viewing or analysis.
 As well as options to export as GIF, JPEG, EPS and FITS,
 there is also the option to transmit the FITS image to one or
-all applications listening using the SAMP or PLASTIC tool interoperability 
+all applications listening using the SAMP tool interoperability
 protocol which will receive images.
-In this way you can transmit the image directly to SAMP/PLASTIC-aware
+In this way you can transmit the image directly to SAMP-aware
 image manipulation tools such as GAIA or Aladin.
 See <ref id="interop"/> for more information about tool interoperability.
 </p>
diff --git a/src/main/uk/ac/starlink/topcat/ControlWindow.java b/src/main/uk/ac/starlink/topcat/ControlWindow.java
index 2608b4c..9756724 100644
--- a/src/main/uk/ac/starlink/topcat/ControlWindow.java
+++ b/src/main/uk/ac/starlink/topcat/ControlWindow.java
@@ -85,7 +85,6 @@ import javax.swing.event.TableModelListener;
 import javax.swing.table.TableColumnModel;
 import org.astrogrid.samp.client.DefaultClientProfile;
 import uk.ac.starlink.auth.AuthManager;
-import uk.ac.starlink.plastic.PlasticUtils;
 import uk.ac.starlink.table.DescribedValue;
 import uk.ac.starlink.table.RowRunner;
 import uk.ac.starlink.table.StarTable;
@@ -105,7 +104,6 @@ import uk.ac.starlink.table.storage.MonitorStoragePolicy;
 import uk.ac.starlink.topcat.activate.ActivationWindow;
 import uk.ac.starlink.topcat.contrib.basti.BaSTITableLoadDialog;
 import uk.ac.starlink.topcat.contrib.gavo.GavoTableLoadDialog;
-import uk.ac.starlink.topcat.interop.PlasticCommunicator;
 import uk.ac.starlink.topcat.interop.SampCommunicator;
 import uk.ac.starlink.topcat.interop.TopcatCommunicator;
 import uk.ac.starlink.topcat.interop.Transmitter;
@@ -1569,10 +1567,6 @@ public class ControlWindow extends AuxWindow
             logger_.info( "Run with no interop" );
             return null;
         }
-        else if ( "plastic".equals( interopType_ ) ) {
-            logger_.info( "Run in PLASTIC mode by request" );
-            return new PlasticCommunicator( control );
-        }
         else {
             final String msg;
             if ( "samp".equals( interopType_ ) ) {
diff --git a/src/main/uk/ac/starlink/topcat/Driver.java b/src/main/uk/ac/starlink/topcat/Driver.java
index 295aa48..a2100e6 100644
--- a/src/main/uk/ac/starlink/topcat/Driver.java
+++ b/src/main/uk/ac/starlink/topcat/Driver.java
@@ -18,8 +18,6 @@ import org.astrogrid.samp.client.DefaultClientProfile;
 import org.astrogrid.samp.client.SampException;
 import org.astrogrid.samp.httpd.UtilServer;
 import uk.ac.starlink.auth.AuthManager;
-import uk.ac.starlink.plastic.PlasticHub;
-import uk.ac.starlink.plastic.PlasticUtils;
 import uk.ac.starlink.table.DefaultValueInfo;
 import uk.ac.starlink.table.DescribedValue;
 import uk.ac.starlink.table.ValueInfo;
@@ -153,7 +151,7 @@ public class Driver {
               pre + " [-help] [-version]"
             + pad + " [-stilts <stilts-args>|-jsamp <jsamp-args>]"
             + pad + " [-verbose] [-debug] [-demo] [-running] [-memory|-disk]"
-            + pad + " [-[no]hub|-exthub|-noserv] [-samp|-plastic]"
+            + pad + " [-[no]hub|-exthub|-noserv] [-samp]"
             + pad + " [[-f <format>] table ...]";
 
         /* Standalone execution (e.g. System.exit() may be called). */
@@ -242,15 +240,6 @@ public class Driver {
                 interopServe = true;
                 ControlWindow.interopType_ = "samp";
             }
-            else if ( arg.equals( "-plastic" ) ) {
-                it.remove();
-                interopServe = true;
-                ControlWindow.interopType_ = "plastic";
-            }
-            else if ( arg.equals( "-noplastic" ) ) { // deprecated
-                it.remove();
-                interopServe = false;
-            }
             else if ( arg.startsWith( "-noserv" ) ) {
                 it.remove();
                 interopServe = false;
@@ -578,8 +567,6 @@ public class Driver {
            .append( p2 + "-disk          use disk backing store for "
                                          + "large tables" ) 
            .append( p2 + "-samp          use SAMP for tool interoperability" )
-           .append( p2 + "-plastic       use PLASTIC for "
-                                         + "tool interoperability" )
            .append( p2 + "-[no]hub       [don't] run internal "
                                          + "SAMP/PLASTIC hub" )
            .append( p2 + "-exthub        run external SAMP/PLASTIC hub" )
